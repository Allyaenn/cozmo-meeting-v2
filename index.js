// Requirements
const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const dashboard = require('./dashboard_engine');
const fs = require('fs');
const util = require('util');
const bodyParser = require('body-parser');
const logger = require('./logger');

//Creating a static route to access material design and jquery
app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded());

/**SETTING UP VARIABLES***************************************************************/ 

//Reading params files
let rawdata = fs.readFileSync('params.json');
let params = JSON.parse(rawdata);

// Variables initialization
let port = params.port;
let number_of_users = params.number_of_users;

// Setting up data structure and computing coordinates

let xc=50, yc=50, xa=50, ya=20, xdelt=xa-xc, ydelt=ya-yc;
let alpha = 2.0*Math.PI/number_of_users;
let beta = Math.atan2(ydelt,xdelt);

let usertab = [];
for(let i = 0; i < number_of_users; i++){
    if (i == 0){
      usertab[i] = {num:i, taken:false, username:"none", pos_x:xa, pos_y:ya, color:"#d12332"};
    }
    else{
    let xt= 30.0*Math.cos((i)*alpha+beta)+50;
    let yt = 30.0*Math.sin((i)*alpha+beta)+50;
    usertab[i] = {num:i, taken:false, username:"none", pos_x:xt, pos_y:yt};
    if (i == 1)
      usertab[i].color = "#0e73aa";
    else if (i == 2)
      usertab[i].color = "#ffd400";
    else if (i == 3)
      usertab[i].color = "#72a53B";
    }
}

dashboard.init(usertab);

/**SOCKET MANAGEMENT***************************************************************/ 

// Socket opening
server.listen(process.env.PORT || 5000, function(){
  logger.logOperation("WEB", "I am online on: "+ port +" and ready for "+number_of_users+" users !","");
});

// Socket handlers
io.on('connection', function (socket) {

  socket.emit('data', {tab: usertab});

  socket.on('pos_taken', function (data){
    logger.logOperation("WEB","a seat was taken : " + data,"");
    data_parsed = JSON.parse(data);
    usertab[data_parsed.pos_id].taken = data_parsed.taken;
    io.emit('data+refresh', {tab: usertab});
  });

  socket.on('auth', function (data){
    usertab[data.pos_id].username = data.username;
    io.emit('update_username', {tab: usertab});

    // checking if everyone is connected 
    if (iseveryoneHere()){
      io.emit('data+table_is_full', {tab: usertab});
    }
    io.emit('data+refresh', {tab: usertab});
  });

  socket.on('message', function (data){
    //console.log("a button was clicked : " + data);
    let feel = JSON.parse(data);  
    dashboard.pushFeelingToDashboard(feel);
    dashboard.garbageCollector();
  });

  socket.on('python', function (data){
    console.log(data);
    io.emit("python_connected", {});
  });

  socket.on('click_log', function (data){
    logger.logOperation("INTERFACE", "a click was received", util.inspect(data, false, null, true));
  });


});

/**ROUTING***************************************************************/ 

// Routing
app.get('/', function(req, res, next){ 
  let options = {}
  // absolute path for the file
  let  fileName = path.resolve('public/view.html');
  res.sendFile(fileName, options, function (err){
    logger.logOperation("WEB", "page loaded","");
    if (err) {
      next(err)
    } else {
      //console.log('Sent:', fileName)
    }
  })
});

app.get('/cl', function(req, res, next){

    let options = {}
    let  fileName = path.resolve('public/clear.html');
    res.sendFile(fileName, options, function (err){
      if (err) {
        next(err)
      } else {
        //console.log('Sent:', fileName)
      }
    })
  });

app.get('/msg', function(req, res, next){

  dashboard.updateTimeLastSend(new Date());
  msgToSend = dashboard.selectMessageWithPriority()
  if (msgToSend == null)
    res.json(null);
  else
    res.json(convertUsernameToChar(msgToSend));
});

app.get('/pageadmin', function(req, res, next){

  let options = {}
  let  fileName = path.resolve('public/admin.html');
  res.sendFile(fileName, options, function (err){
    if (err) {
      next(err)
    } else {
      //console.log('Sent:', fileName)
    }
  })
});

app.post('/', function (req, res) {

  if (req.body.type == "reset_dashboard"){
    let options = {}
    let  fileName = path.resolve('public/admin.html');  
    res.sendFile(fileName, options, function (err){
      if (err) {
        next(err)
      } else {
        //console.log('Sent:', fileName)
        dashboard.deleteDashboardContent();
      }
    })
  }
  else{
      //console.log("Cozmo à terminé : " + util.inspect(req.body, false, null, true));
      logger.logOperation("WEB", "Cozmo a terminé","");
      dashboard.deleteAlreadyExpressedMessages(req.body);
      dashboard.eraseObsoleteMessages();
      res.sendStatus(200);
  }
});

/**VIEW MANAGEMENT METHOD***************************************************************/ 

  // Other Methods 
  function iseveryoneHere(){
    let everyoneIsHere = true;

    usertab.forEach(function(usr, index, array) {
      if (usr.username == "none"){
        everyoneIsHere = false;
      }
     });

    return everyoneIsHere;
  }

  /**OTHERs********************************************************************************/

  function convertUsernameToChar(msg){
    for(let i = 0; i < msg.targets.length; i++){
      for (let j = 0; j < usertab.length; j++){
        if(usertab[j].username == msg.targets[i]){
          msg.targets[i] = String.fromCharCode(65 + j);
        }
      }
    }
    return msg;
  }
  

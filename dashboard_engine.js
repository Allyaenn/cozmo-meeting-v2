const util = require('util');
const mu = require('./my_utils');
const logger = require('./logger');

let messages;

module.exports = {

    isEmpty : function(){
        if (messages.length == 0)
            return true;
        else 
            return false
    },

    selectMessageWithPriority(){

        let selected_msg = null;
        messages.forEach(function(msg, index, array){
            if (msg.weight > 1 && msg.status != "obsolete"){
                if (selected_msg == null){
                    selected_msg = msg;
                }
                else {
                    if ((selected_msg.weight < msg.weight) || (selected_msg.weight == msg.weight && selected_msg.timestamp > msg.timestamp)){
                        selected_msg = msg;
                    }
                } 
            }           
        });

        if (selected_msg != null)
            selected_msg.status = "obsolete";
        logger.logOperation("TRANSMISSION", "Message with highest priority transmitted to the robot", util.inspect(selected_msg, false, null, true));
        return selected_msg;
    },

    pushFeelingToDashboard : function(feeling){
        let type_msg;
        let users_obj = new Set([feeling.username]);

        // types : groupe, participant, participant+aspect, groupe+aspect, aspect, general

        if (feeling.targets.length == 1){
            if (feeling.targets[0] == "groupe"){
                type_msg = "groupe";
            }
            else{
                type_msg = "participant";
            }
        }
        else if (feeling.targets.length == 2){
            if ((feeling.targets.includes("theme") || feeling.targets.includes("comportement") || feeling.targets.includes("temps")) && feeling.targets.includes("groupe")){
                type_msg = "groupe+aspect";
            }
            else if ((feeling.targets.includes("theme") || feeling.targets.includes("comportement") || feeling.targets.includes("temps")) && !feeling.targets.includes("groupe")){
                type_msg = "participant+aspect";
            }
            else{
                type_msg = "participant";
            }
        }

        let message = {
        timestamp : feeling.timestamp,
        targets : feeling.targets,
        emotion : feeling.emotion,
        weight : 1,
        users : users_obj,
        status : "new",
        type : type_msg
        }

        messages.push(message);
        logger.logOperation("FEELING","New feeling expressed",util.inspect(message, false, null, true));
    },

    garbageCollector : function(){
    //on fait plusieurs parcours pour appliquer les règles une par une
    logger.logOperation("DASHBOARD", "Content of the dashboard before GC @ " + new Date() + "\n", util.inspect(messages, false, null, true));

    //console.log(util.inspect(messages, false, null, true));

    this.deleteOldMessages();

    //console.log(messages);

    this.agregateIdenticalMessages();

    //console.log(util.inspect(messages, false, null, true));

    this.agregrateSameTypeMessages();

    //console.log(util.inspect(messages, false, null, true));

    this.deleteContradictoryMessages();

    //console.log(messages);

    // filtering out obsolete messages
    this.eraseObsoleteMessages();
    
    //console.log(util.inspect(messages, false, null, true));
    logger.logOperation("DASHBOARD", "Content of the dashboard after GC @ " + new Date() + "\n", util.inspect(messages, false, null, true));
    },

    // les messages vieux de plus de 3 minutes sont supprimés
    deleteOldMessages: function(){

    messages.forEach(function(msg, index, array){
        let dff = new Date() - new Date(msg.timestamp);
        let three_min = new Date(0).setMinutes(3);
        if (dff >= three_min){
        msg.status = "obsolete";
        logger.logOperation("ENGINE","R1 - Timeout : message deleted", util.inspect(msg, false, null, true));
        }
    });
    logger.logOperation("ENGINE", "R1 - Timeout : applied", "");
    },

    // les messages identiques s'additionnent et les priorités aussi (si les utilisateurs sont différents)
    agregateIdenticalMessages : function(){

        let that = this;
        messages.forEach(function(msg, index, array){
            //le message de référence ne doit pas être obsolète
            if (msg.status != "obsolete"){
                let usr_temp;
                usr_temp = new Set();
                // on sauvegarde l'utilisateur
                usr_temp = mu.mergeSets(usr_temp,msg.users);

                let msgs_matched = false;
                let addition_of_a_new_issuer = false;
                let previous_size_of_usr_temp;
                // mise en place des variables permettant de stocker les caractérisiques du futur message agrégé
                let temps = new Date(msg.timestamp);      

                messages.forEach(function(msg2, index, array){

                    // si le msg n'est pas obsolète, présente la même émotion et la même cible que msg et provient d'un user pas encore pris en compte
                    if (msg != msg2 && msg2.status != "obsolete" && msg.emotion == msg2.emotion && msg.type == msg2.type && that.areIdenticalTargets(msg.targets, msg2.targets)){
                    // -> mais ça marche pas si on a plusieurs users -> il faut travailler avec des tableaux tout le long
                    msgs_matched = true;
                    //mise a jour des caractéristiques permettant de stocker les caractérisiques du futur message agrégé
                    if (new Date(msg2.timestamp) > temps){
                        temps = new Date(msg2.timestamp)
                    }
                    //marque de msg2 comme obsolète
                    msg2.status = "obsolete";
                    logger.logOperation("ENGINE","R2 - IA : agregated message deleted", util.inspect(msg2, false, null, true));
                    //sauvegarde de l'utilisateur
                    previous_size_of_usr_temp = usr_temp.size;
                    usr_temp = mu.mergeSets(usr_temp,msg2.users);

                    if (previous_size_of_usr_temp < usr_temp.size)
                        addition_of_a_new_issuer = true;
                }
            });

            // si des messages analogues on été detecté, mise a jour de msg
            if (msgs_matched){
                msg.weight = that.countUsers(usr_temp, msg.type);
                msg.timestamp = temps;
                msg.users = usr_temp;

                //ajout d'un emetteur
                if (addition_of_a_new_issuer)
                    logger.logOperation("ENGINE","R2 - IA : remaining message uptaded (addition of a new issuer)", util.inspect(msg, false, null, true));
                //simple update
                else
                    logger.logOperation("ENGINE","R2 - IA : remaining message uptaded (feeling repeated from a previous issuer)", util.inspect(msg, false, null, true));
            }
            }
        });
    logger.logOperation("ENGINE", "R2 - IA : applied", "");
    
    },

    //les messages de même groupe (group et meeting statements) et de même émotion s'aggrègent (les utilisateurs sont compté une fois par cible) double boucle avec sauvegarde des utilisateurs
    agregrateSameTypeMessages: function(){
        let that = this;
        //création des structures pour sauvegarder les users
        messages.forEach(function(msg, index, array){

            let users_temps = new Set();
            // le message ne doit pas être obsolète être un "participant-statement"
            if (msg.status != "obsolete" && (msg.type == "participant" || msg.type == "participant+aspect")){
            // pour savoir si on a trouvé des elements à aggréger
            let msgs_matched_STA = false;
            let previous_size_of_usr_set;
            let temps = new Date(msg.timestamp);
            // les users du message de référence sont sauvegardés
            users_temps = mu.mergeSets(users_temps, msg.users);
            // on compare à tous les autres messages
            messages.forEach(function(msg2, index, array){

                // on cherche les messages du même type que msg (PS ou PSA), qui ne sont pas obsolète et qui ont la même émotion et le même type
                if (msg != msg2 && msg2.status != "obsolete" && msg.emotion == msg2.emotion && msg.type == msg2.type && that.isApectIdentical(msg.targets, msg2.targets)){
                    msgs_matched_STA = true;
                    if (new Date(msg2.timestamp) > temps){
                        temps = new Date(msg2.timestamp)
                    }
                    msg2.status = "obsolete";
                    logger.logOperation("ENGINE","R3 - STA (several PS) : agregated message deleted", util.inspect(msg2, false, null, true));
                    users_temps = mu.mergeSets(users_temps, msg2.users);
                }
            });

            // si on a trouvé des messages permettant de faire des aggrégation on insère le nouveau message 
            if (msgs_matched_STA){
                let doublon = false;
                //on vérifie si y a pas déjà un message identique vu qu'on change de type
                //mais si y avait déjà un GS qui matche j'aurais pas de PS qui se balade tout seul...
                messages.forEach(function(msg3, index, array){
                    if (msg != msg3 && msg3.status != "obsolete" && msg.emotion == msg3.emotion && (msg3.target == "groupe" || msg3.target == "groupe+aspect") && that.isApectIdentical(msg.targets, msg3.targets)){
                        //mise à jour du message existant   
                        previous_size_of_usr_set = msg3.users.size;
                        msg3.timestamp = new Date(temps);
                        msg3.users = mu.mergeSets(msg3.users, users_temps);
                        msg3.weight = that.countUsers(msg3.users, msg3.type);
                        if (previous_size_of_usr_set <  msg3.users.size)
                            logger.logOperation("ENGINE","R3 - STA (several PS) : existing message updated (addition of a new issuer)", util.inspect(msg3, false, null, true));
                        else
                            logger.logOperation("ENGINE","R3 - STA (several PS) : existing message updated (feeling repeated from a previous issuer)", util.inspect(msg3, false, null, true));
                        doublon = true;
                        return true; // pour sortir du foreach
                    }
                });

                if (!doublon){
                    let users_obj = users_temps;
                    let new_targets = that.createNewTargets(msg.targets);
                    let type_tmp = "";
                    if (msg.type == "participant")
                        type_tmp = "groupe";
                    else
                        type_tmp = "groupe+aspect";

                    let message = {
                        emotion: msg.emotion,
                        targets: new_targets,
                        type : type_tmp,
                        timestamp: new Date(temps),
                        weight: that.countUsers(users_obj, type_tmp),
                        status : "new",
                        users : users_obj
                    }
                    messages.push(message);
                    logger.logOperation("ENGINE","R3 - STA (several PS) : new agregated message created",util.inspect(message, false, null, true));
                }
                // on marque le message considéré comme obsolète car il fera partie d'un message agrégé
                msg.status = "obsolete";
            }
            // si il n'y a pas de 2ème PS, il faut voir si il y a des GS 
            else if(!msgs_matched_STA){
                messages.forEach(function(msg3, index, array){
                    if (msg != msg3 && msg3.status != "obsolete" && msg.emotion == msg3.emotion && (msg3.type == "groupe" || msg3.type == "groupe+aspect") && that.isApectIdentical(msg.targets, msg3.targets)){
                        //mise à jour du message existant
                        previous_size_of_usr_set = msg3.users.size;
                        msg3.timestamp = new Date(temps);
                        msg3.users = mu.mergeSets(msg3.users, users_temps);
                        msg3.weight = that.countUsers(msg3.users, msg3.type);
                        doublon = true;
                        msg.status = "obsolete";

                        logger.logOperation("ENGINE","R3 - STA (1 PS & 1 GS) : agregated message deleted", util.inspect(msg, false, null, true));
                        if (previous_size_of_usr_set <  msg3.users.size)
                            logger.logOperation("ENGINE","R3 - STA (1 PS & 1 GS) : existing message updated (addition of a new issuer)", util.inspect(msg3, false, null, true));
                        else
                            logger.logOperation("ENGINE","R3 - STA (1 PS & 1 GS) : existing message updated (feeling repeated from a previous issuer)", util.inspect(msg3, false, null, true));
                    }
                });
            }
            }
        });
        logger.logOperation("ENGINE", "R3 - STA : applied", "");
    },

    // si il y a des messages avec des émotions différentes sur la même cible, seul le dernier est considéré/conservé 
    deleteContradictoryMessages: function(){
        let that = this;
        messages.forEach(function(msg, index, array){

            if (msg.weight > (1) && msg.status != "obsolete"){

                messages.forEach(function(msg2, index, array){

                    if (msg2.weight > (1) && msg2.status != "obsolete"){

                        if (msg != msg2 && msg.emotion != msg2.emotion && that.areIdenticalTargets(msg.targets, msg2.targets)){

                            if (msg.timestamp <= msg2.timestamp && that.areSourcesOverlappingEnough(msg, msg2)){
                                msg.status = "obsolete"
                                logger.logOperation("ENGINE","R4 - CONTRADICTION : contradictory message deleted", util.inspect(msg, false, null, true));
                            }
                            else if(msg.timestamp > msg2.timestamp && that.areSourcesOverlappingEnough(msg2, msg)){
                                msg2.status = "obsolete"
                                logger.logOperation("ENGINE","R4 - CONTRADICTION : contradictory message deleted", util.inspect(msg2, false, null, true));
                            }
                        }
                    }
                });
            }
        });
        logger.logOperation("ENGINE", "R4 - Contradiction : applied", "");
    },

    deleteAlreadyExpressedMessages : function(last_sent_message){
        let that = this;
        // console.log("Is there obsolete messages ?\n");
        // console.log("Last sent message : \n" + util.inspect(last_sent_message, false, null, true));
        // console.log("time : " + this.time_last_send);
        messages.forEach(function(msg, index, array){
            //console.log("Current message : \n" + util.inspect(msg, false, null, true));
            if (msg.emotion == last_sent_message.emotion && that.areIdenticalTargets(msg.targets, last_sent_message.targets) && Date.parse(msg.timestamp) > that.time_last_send){
                msg.status = "obsolete";
                logger.logOperation("ENGINE"," R5 - DAEM : Message already expressed just now", util.inspect(msg, false, null, true));
            }
        });
        logger.logOperation("ENGINE", "R5 - DAEM : applied", "");
    },

    eraseObsoleteMessages: function(){
    let recentMsgs = messages.filter(function(e){
        return e.status != "obsolete";
    });
    messages = recentMsgs;
    }, 

    /**OTHER METHODs***************************************************************/ 

    areIdenticalTargets: function(tar1, tar2){
    if (tar1.length == tar2.length){

        tar1.sort();
        tar2.sort();

        for(let i = 0; i < tar1.length; i++){
        if (tar1[i] != tar2[i])
            return false;
        }
        return true;
    }
    return false
    },

    isApectIdentical: function(tar1, tar2){

        if (tar1.indexOf("theme") != -1 && tar2.indexOf("theme") != -1)
            return true;
        else if (tar1.indexOf("temps") != -1 && tar2.indexOf("temps") != -1)
            return true;
        else if (tar1.indexOf("comportement") != -1 && tar2.indexOf("comportement") != -1)
            return true;
        else if (tar1.indexOf("theme") == -1 && tar1.indexOf("temps") == -1 && tar1.indexOf("comportement") == -1 
              && tar2.indexOf("theme") == -1 && tar2.indexOf("temps") == -1 && tar2.indexOf("comportement") == -1) 
            return true;
        else
            return false;
    },

    areSourcesOverlappingEnough: function(msg1, msg2){
        // let sources1 = mu.listDistinctItems(msg1.users, msg1.type);
        // let sources2 = mu.listDistinctItems(msg2.users, msg2.type);
        let nb_urs1 = msg1.users.size;
        let nb_urs2 = msg2.users.size;

        let max_nb_users = Math.max(nb_urs1, nb_urs2)
        let nb_sim_items = mu.countSimilarItemsInSets(msg1.users, msg2.users);
        if(nb_sim_items >= 0.5*max_nb_users)
            return true;
        else
            return false;
    },

    // let sources1 = mu.listDistinctItems(msg1.users, msg1.type);
    //     let sources2 = mu.listDistinctItems(msg2.users, msg2.type);
    //     let nb_urs1 = sources1.size;
    //     let nb_urs2 = sources2.size;

    //     let max_nb_users = Math.max(nb_urs1, nb_urs2)
    //     let nb_sim_items = mu.countSimilarItemsInSets(sources1, sources2);
    //     if(nb_sim_items >= 0.5*max_nb_users)
    //         return true;
    //     else
    //         return false;

    countUsers: function(users, type){
        return users.size;
    },

    createNewTargets : function(tab){  
        let aspect_tmp = "";
        for(let i = 0; i < tab.length; i++){
            if(tab[i] === "theme" || tab[i] === "temps" || tab[i] === "comportement"){
                aspect_tmp = tab[i];
                console.log(tab[i])
                return ["groupe", aspect_tmp];
            }
        }
        return ["groupe"];
    },

    // if (tab.length == 1){
    //     return ["groupe"];
    // }
    // else{
    //     let aspect_tmp = "";
    //     for(let i = 0; i < tab.length; i++){
    //     if(tab[i] == "theme" || tab[i] == "temps" || tab[i] == "comportement")
    //         aspect_tmp = tab[i];
    //     }
    //     return ["groupe", aspect_tmp];
    // }

    extractUsername : function(tab){
        let username_tmp = "";
    
        if(tab.length == 1){
            username_tmp = tab[0];
        } 
        else if(tab.length == 2){
            for(let i = 0; i < tab.length; i++){
            if(tab[i] != "theme" && tab[i] != "temps" && tab[i] != "comportement")
                username_tmp = tab[i];
            }
        }
        return username_tmp;
    },

    updateTimeLastSend : function(new_timestamp){
        this.time_last_send = new_timestamp;
    },

    deleteDashboardContent : function(){
        messages = [];
        logger.logOperation("DASHBOARD","Content reset executed : ",  JSON.stringify(messages));
    },

    init : function(usertab_orig){
        messages = [];
        usertab = usertab_orig;
        logger.init();
        var self = this;
        setInterval(function(){
            if (typeof messages !== 'undefined' && messages.length > 0){
                self.deleteOldMessages();
                self.eraseObsoleteMessages()
                //logger.logOperation("ENGINE", "R1 - Timeout : applied and messages cleaned", "");
            }
            else
                logger.logOperation("ENGINE", "R1 - Timeout : applied and messages is empty", "");

        }, 30000);
    }
}


const util = require('util');
module.exports = {

    arrayInArray : function (tab1, tab2){

        let elements_matched = false;
        tab1.forEach(function(el1, index, array){
        tab2.forEach(function(el2, index, array){
            if (el1 == el2)
            elements_matched = true;
        });
        });
        
        return elements_matched;
    
    },
    
    addArrayInSet : function addArrayInSet(set, arr){
        arr.forEach(function(el, index, array){
            set.add(el);
        });
    },


    mergeSets : function(set1, set2){

        let final_set = new Set();
    
        set1.forEach(function(el1, index, array){
            final_set.add(el1);
        });
    
        set2.forEach(function(el2, index, array){
            final_set.add(el2);
        });
    
        return final_set;
    }, 

    countSimilarItemsInSets(set1, set2){
        // console.log(util.inspect(set1, false, null, true));
        // console.log(util.inspect(set2, false, null, true));
        let nb = 0;
        set1.forEach(function(el1, index, array){
            set2.forEach(function(el2, index, array){
                if (el1 == el2)
                    nb++;
            });
        });
        //console.log("NB SIIS : " + nb);
        return nb;
    }, 


    listDistinctItems: function(users, type){
        let final_users = new Set();
        if (type == "groupe" || type == "groupe+aspect"){
            let val_arr = Object.values(users);
            val_arr.forEach(function(set, index, array){
                set.forEach(function(el, index, array){
                    final_users.add(el);
                });
            });
            return final_users;
        }
        return users
    }
}
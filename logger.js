// creating log rep and files

const fs = require('fs');

let curr_time_str, folder_path, log_file_f_path, log_file_eng_path, log_file_dash_path, log_file_tr_path;

module.exports = {

init : function (){
    let curr_time = new Date();

    curr_time_str = curr_time.getFullYear()+'_'+curr_time.getMonth()+'_'
                      +curr_time.getDate()+'-'+curr_time.getHours()+'_'
                      +curr_time.getMinutes()+'_'+curr_time.getSeconds();
    
    folder_path = 'logs/logs_'+ curr_time_str;
    log_file_f_path = folder_path+'/feelings_'+ curr_time_str + '.txt';
    log_file_eng_path = folder_path+'/engine_decisions_'+ curr_time_str + '.txt';
    log_file_dash_path = folder_path+'/dashboard_state_'+ curr_time_str + '.txt';
    log_file_tr_path = folder_path+'/messages_transmitted_'+ curr_time_str + '.txt';
    log_file_pi_path = folder_path+'/personnal_interface_'+ curr_time_str + '.txt';
    log_file_w_path = folder_path+'/web_'+ curr_time_str + '.txt';

    fs.mkdirSync(folder_path);
    
    fs.writeFileSync(log_file_f_path,'');
    fs.writeFileSync(log_file_eng_path,'');
    fs.writeFileSync(log_file_dash_path,'');
    fs.writeFileSync(log_file_tr_path,'');
    fs.writeFileSync(log_file_pi_path,'');
    fs.writeFileSync(log_file_w_path,'');
},

logOperation : function(type, comment, str){

    let log_str = str + "\n";

    switch (type) {
        case 'FEELING':
            fs.appendFileSync(log_file_f_path, comment + log_str);
            break;
        case 'ENGINE':
            fs.appendFileSync(log_file_eng_path, comment + log_str);
            break;
        case 'DASHBOARD':
            fs.appendFileSync(log_file_dash_path, comment + log_str);
            break;
        case 'TRANSMISSION':
            fs.appendFileSync(log_file_tr_path, comment + log_str);
            break;
        case 'INTERFACE':
            fs.appendFileSync(log_file_pi_path, comment + log_str);
            break;
        case 'WEB':
            fs.appendFileSync(log_file_w_path, comment + log_str);
            break;
        default:
            console.log("ERROR : LOG TYPE UNKNOWN");
      }

    //if (type != "INTERFACE")
    console.log (type +" - "+ comment +"\n" + log_str);
  }
} 